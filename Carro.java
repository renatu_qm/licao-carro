public class Carro {

    private Integer quantidadePortas;
    private Integer anoFabricacao;
    private String numeroChassi;
    private String combustivel;
    private String cor;



    public Carro(Integer quantidadePortas, Integer anoFabricacao, String chassi, String comum, String cor){

        this.quantidadePortas(quantidadePortas);
        this.anoFabricacao(anoFabricacao);
        this.numeroChassi(numeroChassi);
        this.combustivel(combustivel);
        this.cor(cor);
    }


    public Integer getQuantidadePortas(){
        return quantidadePortas;
    }

    public void quantidadePortas(Integer quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }


    public int getAnoFabricacao(){
        return anoFabricacao;
    }

    public void anoFabricacao(Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public String getNumeroChassi() {
        return numeroChassi = "9BRBLWHEXG0107721";
    }

    public void numeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
    }


    public String getCombustivel(){
        return combustivel = "Comum";
    }

    public void combustivel(String combustivel) {
        this.combustivel = combustivel;
    }


    public String getCor(){
        return cor = "Azul";
    }

    public void cor(String cor){
        this.cor = cor;
    }


    public void imprimeValores(){
        System.out.println("Caracteristicas-> ");
        System.out.println("Quantidade de portas: " + getQuantidadePortas());
        System.out.println("Ano de fabricação: " + getAnoFabricacao());
        System.out.println("Número do Chassi: " + getNumeroChassi());
        System.out.println("Combustível: " + getCombustivel());
        System.out.println("Cor: " + getCor());
   }
}
